<?php

/**
 * @file
 * Contains FeedsPartyProcessor.
 */

/**
 * Feeds processor plugin. Create party from feed items.
 */
class FeedsPartyProcessor extends FeedsProcessor {

  /**
   * Define entity type.
   */
  public function entityType() {
    return 'party';
  }

  /**
   * Implements parent::entityInfo().
   */
  protected function entityInfo() {
    $info = parent::entityInfo();
    $info['label plural'] = t('Parties');
    return $info;
  }

  /**
   * Creates a new Party in memory and returns it.
   */
  protected function newEntity(FeedsSource $source) {
    $party = party_create();
    return $party;
  }


  /**
   * Validates a party entity.
   */
  protected function entityValidate($party) {
    parent::entityValidate($party);

    if (empty($party->label) || empty($party->email) || !valid_email_address($party->email)) {
      throw new FeedsValidationException(t('Party label is missing or email not valid.'));
    }
  }

  /**
   * Save a party entity.
   */
  protected function entitySave($party) {
    party_save($party);
  }

  /**
   * Delete multiple party entity.
   */
  protected function entityDeleteMultiple($pids) {
    party_delete_multiple($pids);
  }

  /**
   * Return available mapping targets.
   */
  public function getMappingTargets() {
    // @todo get the data from database.
    $targets = array(
      'label' => array(
        'name' => t('Party Label'),
        'description' => t('Party Label.'),
       ),
      'email' => array(
        'name' => t('Party Email'),
        'description' => t('Email address of the party.'),
       ),
    );

    foreach (party_get_data_set_info() as $data_set_name => $info) {
      if ($info['entity type'] == 'profile2') {
        $bundle_fields = field_info_instances($info['entity type'], $info['entity bundle']);
        foreach ($bundle_fields as $field) {
          $targets += array(
            implode(':', array($info['entity type'], $info['entity bundle'], $field['field_name'])) => array(
            // @todo Discuss 'Party' name.
              'name' => 'Party ' . t($field['label']),
              'description' => t($field['description'])
             )
          );
        }
      }
    }

    $this->getHookTargets($targets);

    return $targets;
  }


  public function process(FeedsSource $source, FeedsParserResult $parser_result) {
    foreach ($parser_result->items as $item) {
      $party = party_create(array(
        'label' => $item['party-label'],
        'email' => $item['email']
      ));
      $party->save();

      foreach ($this->config['mappings'] as $mapping) {
        if (strpos($mapping['target'], 'profile2:') === 0) {
          list($entity_type, $bundle, $field_name) = explode(':', $mapping['target']);
          $profiles[$bundle][$field_name] = $item[strtolower($mapping['source'])];
        }
      }

      // Save or update each profile.
      foreach ($profiles as $bundle => $values) {
        $profile = profile2_create(array(
          'type' => $bundle,
          'uid' => NULL, // Do not link with user account.
        ));

        // Load entity wrapper.
        $profile_wrapper = entity_metadata_wrapper('profile2', $profile);

        // Set mapped field values.
        foreach ($values as $field_name => $value) {
          $profile_wrapper->{$field_name}->set($value);
        }

        // Save profile.
        $profile_wrapper->save();
        party_attach_entity($party, $profile, 'profile2_' . $bundle);
        $party->save();
      }
    }
  }
}

